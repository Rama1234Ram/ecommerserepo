package com.web.ecommerce.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.web.ecommerce.appconstants.ApplicationConstants;
import com.web.ecommerce.dto.ResponseDto;
import com.web.ecommerce.dto.SearchProductInformation;
import com.web.ecommerce.dto.logindto;
import com.web.ecommerce.entity.Products;
import com.web.ecommerce.entity.Users;
import com.web.ecommerce.exception.InvalidUser;
import com.web.ecommerce.exception.ProductException;
import com.web.ecommerce.repository.UserRepository;
import com.web.ecommerce.repository.productsRepository;
import com.web.ecommerce.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepo;

	@Autowired
	productsRepository productRepo;

	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	ModelMapper modelMapper = new ModelMapper();

	@Override
	public ResponseDto login(logindto loginDto) {

		logger.info("validating the user login information in the userserviceimplementation");

		if (loginDto.getUserName() == null || loginDto.getPassword() == null) {
			ResponseDto response = new ResponseDto();
			response.setMessage("please enter the username and password,it cannot be empty");
			response.setCode(HttpStatus.BAD_REQUEST.value());
			return response;
		}

		Users userEntity = userRepo.findByUserName(loginDto.getUserName());

		if (userEntity == null) {
			ResponseDto response = new ResponseDto();
			response.setMessage("please enter valid user details");
			response.setCode(HttpStatus.BAD_REQUEST.value());

			return response;

		}

		if (loginDto.getUserName().equals(userEntity.getUserName())
				&& loginDto.getPassword().equals(userEntity.getPassword())) {
			ResponseDto vresponse = new ResponseDto();
			vresponse.setMessage("user logined successfully");
			vresponse.setCode(HttpStatus.ACCEPTED.value());

			return vresponse;

		} else {
			throw new InvalidUser(ApplicationConstants.LOGIN_FAILURE);

		}

	}

	@Override
	public List<SearchProductInformation> getProductsBySearh(String productName) {

		if (productName == null) {
			throw new ProductException(ApplicationConstants.PRODUCT_NAME_NULL);
		}

		List<Products> productsInfo = productRepo.findByProductNameContains(productName);

		logger.info("fetching the all products information in user service Impl");
		System.out.println(productsInfo);
		List<SearchProductInformation> searchproducts = new ArrayList<SearchProductInformation>();

		for (Products productInfo : productsInfo) {
			SearchProductInformation searchproduct = modelMapper.map(productInfo, SearchProductInformation.class);

			logger.info("Gather the all production information in model mapper");

			searchproducts.add(searchproduct);
		}

		return searchproducts;
	}

}

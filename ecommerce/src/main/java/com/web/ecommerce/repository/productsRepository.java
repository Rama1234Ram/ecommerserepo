package com.web.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.web.ecommerce.entity.Products;
import java.lang.String;
import java.util.List;

public interface productsRepository extends JpaRepository<Products, Long> {
	
	
	List<Products> findByProductNameContains(String productname);

}

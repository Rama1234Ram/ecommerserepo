package com.web.ecommerce.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "users")
@Getter
@Setter
@ToString
public class Users implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4969468097684460996L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private Long userId;
	private String userName;
	private String emailId;
	private String password;

}

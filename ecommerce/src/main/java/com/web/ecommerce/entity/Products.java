package com.web.ecommerce.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "products")
@Getter
@Setter
@ToString
public class Products {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private Long productId;
	private String productName;
	private String productDescription;
	private String availability;
	private String productCode;

}

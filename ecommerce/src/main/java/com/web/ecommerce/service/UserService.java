package com.web.ecommerce.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.web.ecommerce.dto.ResponseDto;
import com.web.ecommerce.dto.SearchProductInformation;
import com.web.ecommerce.dto.logindto;

@Service
public interface UserService {

	public ResponseDto login(logindto loginDto);

	public List<SearchProductInformation> getProductsBySearh(String productName);

}
